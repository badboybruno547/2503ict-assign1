<?php

class UserController extends \BaseController {

	public function login()
	{
		
		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);
		
		if (Auth::attempt($userdata))
		{
			//echo("successfully logged in");
			//exit;
			return Redirect::secure('/');
		}
		else
		{
			// echo("failed login");
			// exit;
			return Redirect::to(URL::previous())->withInput();
		}
	}
	
	public function logout()
	{
		Auth::logout();
		return Redirect::secure('/');
	}
	
	public function loginform()
	{
		return View::make('layout.login');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$posts = Post::orderBy('created_at', 'DESC')->paginate(8);
		$comments = Comment::all();
		$count = array();
		foreach ($posts as $post)
		{
			$count[] = Comment::where('post_id', '=', $post->id)->count();
		}
		return View::make('layout.home', compact('posts', 'count'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('layout.register');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		echo("got to the store new user method");
		
		$input = Input::all();
		$validator = Validator::make($input, User::$rules);
		
		if($validator->passes())
		{
			$account = array('username' => $input['emailAddress'], 'password' => $input['password']);
			$user = new User;
			$user->firstName = $input['firstName'];
			$user->lastName = $input['lastName'];
			$user->dateOfBirth = $input['dateOfBirth'];
			$user->image = $input['image'];
			$user->userName = $input['emailAddress'];
			$user->password = Hash::make($input['password']);
			$user->save();
			Auth::attempt($account);
			return Redirect::to(secure_url('/'));
		}
		else
		{
			return Redirect::to(URL::previous())->withInput()->withErrors($validator);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);
		$posts = $user->posts()->paginate(8);
		$comments = Comment::all();
		$count = array();
		foreach ($posts as $post)
		{
			$count[] = Comment::where('post_id', '=', $post->id)->count();
		}
		return View::make('layout.viewprofile', compact('posts', 'count', 'user'));
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//echo("got to the edit user route");
		if(Auth::User())
		{
			return View::make('layout.editprofile');
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		echo("update the user");
		$input = Input::all();
		$validator = Validator::make($input, User::$updaterules);
		
		if($validator->passes())
		{
			$user = Auth::user();
			$user->firstName = $input['firstName'];
			$user->lastName = $input['lastName'];
			$user->dateOfBirth = $input['dateOfBirth'];
			$user->image = $input['image'];
			$user->save();
			return Redirect::to(secure_url('/'));
		}
		else
		{
			return Redirect::to(URL::previous())->withInput()->withErrors($validator);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function documentation()
	{
		return View::make('layout.documentation');
	}


}
