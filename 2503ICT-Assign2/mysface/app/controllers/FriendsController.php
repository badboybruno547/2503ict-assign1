<?php

class FriendsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function people()
	{
		$persons = User::orderBy('lastName', 'DESC')->paginate(8);
		return View::make('layout.people', compact('persons'));
	}
	
	public function index()
	{
		$user = Auth::user();
		$friends = User::whereHas('friendsAdded', function($friend) use ($user){
			return $friend->whereRaw('user2_id like ?', array($user->id));
		})->orWhereHas('addedFriends', function($friend) use ($user){
			return $friend->whereRaw('user1_id like ?', array($user->id));
		})->paginate(8);
		return View::make('layout.friendslist', compact('friends'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
		$friend = new Friend();
		if(Auth::User())
		{
			$user = Auth::user();
			$user2 = User::find($id);
			$user->friendsAdded()->save($user2);
			return Redirect::to(URL::previous());
		}
		else
		{
			return Redirect::to(URL::previous());
		}
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		echo('got to the friends details page');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if(Auth::User())
		{
			$user = Auth::User();
			$friends = Friend::all();
			
			foreach ($friends as $friendship)
			{
				if ($friendship->user1_id == $id && $friendship->user2_id == $user->id || $friendship->user2_id == $id && $friendship->user1_id == $user->id)
				{
					$friendship->delete();
				}
			}
			
			return Redirect::to(URL::previous());
		}
		else
		{
			return Redirect::to(URL::previous());
		}
	}


}
