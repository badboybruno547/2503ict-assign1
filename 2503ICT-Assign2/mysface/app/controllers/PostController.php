<?php

class PostController extends \BaseController {

	public function rules()
	{
	    return [
	        'title' => 'required|unique|max:255',
	        'body' => 'required',
	    ];
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validator = Validator::make($input, Post::$rules);
		
		if($validator->passes())
		{
			$post = new Post();
			$post->title = $input['title'];
			$post->message = $input['message'];
			$post->privacy = 'public';
			$post->user()->associate(Auth::user());
			$post->save();
			return Redirect::to(URL::previous());
		}
		else
		{
			return Redirect::to(URL::previous())->withInput()->withErrors($validator);
		}
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = Post::find($id);
		$user = User::where('id', '=', $post->user_id)->first();
		$comments = Comment::where('post_id', '=', $id)->orderBy('created_at', 'DESC')->paginate(8);
		return View::make('layout.view', compact('user', 'post', 'comments'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		if (Auth::user())
		{
			$post = Post::find($id);
			return View::make('layout.edit', compact('post'));
		}
		else
		{
			return Redirect::to(secure_url('/'));
		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$post = Post::find($id);
		$input = Input::all();
		$validator = Validator::make($input, Post::$rules);
		
		if($validator->passes())
		{
			$post->title = $input['title'];
			$post->message = $input['message'];
			$post->privacy = 'public';
			$post->save();
			return Redirect::to(secure_url('/'));
		}
		else
		{
			return Redirect::to(URL::previous())->withInput()->withErrors($validator);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$post = Post::find($id);
		$post->delete();
		return Redirect::to(secure_url('/'));
	}


}
