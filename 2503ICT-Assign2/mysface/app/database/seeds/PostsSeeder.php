<?php

class PostsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$post = new Post;
		$user = User::find(1);
		$post->title = 'Private Message';
		$post->message = 'This is a test to make sure i can see private messages. 321';
		$post->privacy = 'Private';
		$post->user()->associate($user);
		$post->save();
		
		$post = new Post;
		$user = User::find(1);
		$post->title = 'Welcome to the new website';
		$post->message = 'Hi guys, thanks again for using mysface. Take a look at the new look website.';
		$post->privacy = 'Public';
		$post->user()->associate($user);
		$post->save();
		
		$post = new Post;
		$user = User::find(1);
		$post->title = 'Friends Only';
		$post->message = 'This tests the functionality of hidden posts.';
		$post->privacy = 'Friends';
		$post->user()->associate($user);
		$post->save();
		
		$post = new Post;
		$user = User::find(2);
		$post->title = 'Private Message';
		$post->message = 'This is a test to make sure i can see private messages.';
		$post->privacy = 'Private';
		$post->user()->associate($user);
		$post->save();
		
		$post = new Post;
		$user = User::find(2);
		$post->title = 'Welcome to the new website';
		$post->message = 'Hi guys, thanks again for using mysface. Take a look at the new look website.';
		$post->privacy = 'Public';
		$post->user()->associate($user);
		$post->save();
		
		$post = new Post;
		$user = User::find(2);
		$post->title = 'Friends Only';
		$post->message = 'This tests the functionality of hidden posts.';
		$post->privacy = 'Friends';
		$post->user()->associate($user);
		$post->save();
		
		$post = new Post;
		$user = User::find(3);
		$post->title = 'Private Message';
		$post->message = 'This is a test to make sure i can see private messages. 321';
		$post->privacy = 'Private';
		$post->user()->associate($user);
		$post->save();
		
		$post = new Post;
		$user = User::find(3);
		$post->title = 'Assignments due wednesday';
		$post->message = 'Hi guys, dont forget that the assignment is due wednesday.';
		$post->privacy = 'Public';
		$post->user()->associate($user);
		$post->save();
		
		$post = new Post;
		$user = User::find(3);
		$post->title = 'Friends Only';
		$post->message = 'This tests the functionality of hidden posts.';
		$post->privacy = 'Friends';
		$post->user()->associate($user);
		$post->save();
		
		$post = new Post;
		$user = User::find(1);
		$post->title = 'Assignment is hard';
		$post->message = 'God that assignment for programming was hard this week. Thank god that is over.';
		$post->privacy = 'Public';
		$post->user()->associate($user);
		$post->save();
	}	
}