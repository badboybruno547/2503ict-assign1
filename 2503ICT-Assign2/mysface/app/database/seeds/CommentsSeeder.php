<?php

class CommentsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$comment = new Comment;
		$user = User::find(2);
		$post = Post::find(1);
		$comment->user()->associate($user);
		$comment->post()->associate($post);
		$comment->message = 'Comment 1';
		$comment->save();
		
		$comment = new Comment;
		$user = User::find(3);
		$post = Post::find(1);
		$comment->user()->associate($user);
		$comment->post()->associate($post);
		$comment->message = 'Comment 2';
		$comment->save();
		
		$comment = new Comment;
		$user = User::find(4);
		$post = Post::find(1);
		$comment->user()->associate($user);
		$comment->post()->associate($post);
		$comment->message = 'Comment 3';
		$comment->save();
		
		$comment = new Comment;
		$user = User::find(5);
		$post = Post::find(1);
		$comment->user()->associate($user);
		$comment->post()->associate($post);
		$comment->message = 'Comment 4';
		$comment->save();
		
		$comment = new Comment;
		$user = User::find(4);
		$post = Post::find(1);
		$comment->user()->associate($user);
		$comment->post()->associate($post);
		$comment->message = 'Comment 5';
		$comment->save();
		
		$comment = new Comment;
		$user = User::find(2);
		$post = Post::find(1);
		$comment->user()->associate($user);
		$comment->post()->associate($post);
		$comment->message = 'Comment 6';
		$comment->save();
		
		$comment = new Comment;
		$user = User::find(7);
		$post = Post::find(1);
		$comment->user()->associate($user);
		$comment->post()->associate($post);
		$comment->message = 'Comment 7';
		$comment->save();
		
		$comment = new Comment;
		$user = User::find(1);
		$post = Post::find(1);
		$comment->user()->associate($user);
		$comment->post()->associate($post);
		$comment->message = 'Comment 8';
		$comment->save();
		
		$comment = new Comment;
		$user = User::find(6);
		$post = Post::find(1);
		$comment->user()->associate($user);
		$comment->post()->associate($post);
		$comment->message = 'Comment 9';
		$comment->save();
		
		$comment = new Comment;
		$user = User::find(9);
		$post = Post::find(1);
		$comment->user()->associate($user);
		$comment->post()->associate($post);
		$comment->message = 'Comment 10';
		$comment->save();
	}
}