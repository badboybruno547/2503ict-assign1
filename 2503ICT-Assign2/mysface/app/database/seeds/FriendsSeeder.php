<?php

class FriendsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$user = User::find(1);
		$user2 = User::find(2);
		$user->friendsAdded()->save($user2);
		//$user->save();
		
		$user = User::find(1);
		$user2 = User::find(3);
		$user->friendsAdded()->save($user2);
		
		$user = User::find(1);
		$user2 = User::find(4);
		$user->friendsAdded()->save($user2);
		
		$user = User::find(5);
		$user2 = User::find(1);
		$user->friendsAdded()->save($user2);
		
		$user = User::find(6);
		$user2 = User::find(1);
		$user->friendsAdded()->save($user2);
		
		$user = User::find(7);
		$user2 = User::find(1);
		$user->friendsAdded()->save($user2);
	}
}