<?php

class UsersSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$user = new User;
		$user->firstName = 'Brenton';
		$user->lastName = 'Millers';
		$user->dateOfBirth = '1991-02-02';
		$user->userName = 'test1@test.com';
		$user->password = Hash::make('password');
		$user->save();
		
		$user = new User;
		$user->firstName = 'Bradley';
		$user->lastName = 'Gardam';
		$user->dateOfBirth = '1901-01-10';
		$user->userName = 'test2@test.com';
		$user->password = Hash::make('password');
		$user->save();
		
		$user = new User;
		$user->firstName = 'David';
		$user->lastName = 'Chen';
		$user->dateOfBirth = '1972-09-23';
		$user->userName = 'test3@test.com';
		$user->password = Hash::make('password');
		$user->save();
		
		$user = new User;
		$user->firstName = 'Ryan';
		$user->lastName = 'Nicholls';
		$user->dateOfBirth = '1996-10-20';
		$user->userName = 'test4@test.com';
		$user->password = Hash::make('password');
		$user->save();
		
		$user = new User;
		$user->firstName = 'Aubrey';
		$user->lastName = 'Harry';
		$user->dateOfBirth = '1996-10-20';
		$user->userName = 'test5@test.com';
		$user->password = Hash::make('password');
		$user->save();
		
		$user = new User;
		$user->firstName = 'Terry';
		$user->lastName = 'Lambert';
		$user->dateOfBirth = '1991-06-01';
		$user->userName = 'test6@test.com';
		$user->password = Hash::make('password');
		$user->save();
		
		$user = new User;
		$user->firstName = 'Samantha';
		$user->lastName = 'OSullivan';
		$user->dateOfBirth = '1991-06-01';
		$user->userName = 'test7@test.com';
		$user->password = Hash::make('password');
		$user->save();
		
		$user = new User;
		$user->firstName = 'Sharyn';
		$user->lastName = 'Foster';
		$user->dateOfBirth = '1964-06-20';
		$user->userName = 'test8@test.com';
		$user->password = Hash::make('password');
		$user->save();
		
		$user = new User;
		$user->firstName = 'Cameron';
		$user->lastName = 'Treacey';
		$user->dateOfBirth = '1991-01-06';
		$user->userName = 'test9@test.com';
		$user->password = Hash::make('password');
		$user->save();
		
		$user = new User;
		$user->firstName = 'Ben';
		$user->lastName = 'Dean';
		$user->dateOfBirth = '1991-04-07';
		$user->userName = 'test10@test.com';
		$user->password = Hash::make('password');
		$user->save();
	}

}