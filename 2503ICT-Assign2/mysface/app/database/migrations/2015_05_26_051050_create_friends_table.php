<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('friends', function($table)
		{
			$table->increments('id');
			$table->integer('user1_id')->unsigned();
			$table->foreign('user1_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->integer('user2_id')->unsigned();
			$table->foreign('user2_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('friends');
	}
}
