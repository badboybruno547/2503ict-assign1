<!-- This document contains all the additional code to add to the master blade file to create the view page. -->

<!-- imports the master blade file -->
@extends('layout.master')

<!-- imbeds the title of the individial page into the master. -->
@section('title')
Mysface View
@stop

<!-- Inserts the rest of the page into the body of the master page. -->
@section('content')

<!-- Creates the quick links table down the side. -->
<br/>
<div class="row">
    <div class="col-sm-3" id="left">
        <div class="panel panel-primary">
        <div class="panel-heading">Quick Links</div>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{{ URL::secure('/')}}}">Home</a></li>
                <li class="list-group-item"><a href="{{{ URL::secure('friend/people') }}}">People</a></li>
                @if(Auth::user())
                <li class="list-group-item"><a href="{{{ URL::secure('friend/') }}}">Friends</a></li>
                @endif
            </ul>
        </div>
    </div>
    
    <div class="col-sm-8" id="center">
        <div class="panel panel-primary">
            <div class="panel-heading">
                
                @if($post->user->image_file_name != null)
                        <h3 class="panel-title"><img src="{{{ secure_asset($post->user->image->url('thumb'))}}}">  {{{ $user->firstName }}} {{{ $user->lastName }}} posted at: {{{ $post->created_at }}}</h3>
                    @else
                        <h3 class="panel-title"><img src="{{{ secure_asset('images/blankprofilepic.png') }}}" width="100" height ="100">  {{{ $user->firstName }}} {{{ $user->lastName }}} posted at: {{{ $post->created_at }}}</h3>
                    @endif
            </div>
            <div class="panel-body">
                
                <!-- This form contains the original post that is being viewed. Below follows the comments attached to this post. -->
                {{ Form::model($post) }}
                    <div class="form-group">
                        {{ Form::label('title', 'Title:') }}
                        {{ Form::text('title', null, array('class'=>'form-control', 'placeholder'=>'Title of the post.', 'readonly')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('message', 'Message:') }}
                        {{ Form::textarea('message', null, array('class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Insert your message here.', 'readonly')) }}
                    </div>
                {{ Form::close() }}
            </div>
        </div>
        
        <!-- This for loop prints all of the comments liked to the above post. -->
        @foreach ($comments as $comment)
        {{ Form::model($comment, array('method' => 'DELETE', 'url' => secure_url('comment', $comment->id))) }}
         <div class="panel panel-primary">
            <div class="panel-heading">
                @if(Auth::user())
                <button type="submit" class="close" aria-hidden="true" name="action" value="delete">&times;</button>
                @endif
                @if($comment->user->image_file_name != null)
                        <h3 class="panel-title"><a href="{{{ URL::secure('user', $comment->user->id) }}}"><img src="{{{ secure_asset($comment->user->image->url('thumb'))}}}"></a>  {{{ $comment->user->firstName }}} {{{ $comment->user->lastName }}} commented at: {{{ $comment->created_at }}}</h3>
                    @else
                        <h3 class="panel-title"><a href="{{{ URL::secure('user', $comment->user->id) }}}"><img src="{{{ secure_asset('images/blankprofilepic.png') }}}" width="100" height ="100"></a>  {{{ $comment->user->firstName }}} {{{ $comment->user->lastName }}} commented at: {{{ $comment->created_at }}}</h3>
                    @endif
            </div>
            <div class="panel-body">
                
                <!-- This form contains the original post that is being viewed. Below follows the comments attached to this post. -->
                    <div class="form-group">
                    <label>Comment:</label>
                    {{{ $comment->message }}}
                </div>
                
                
            </div>
        </div>
        {{ Form::close() }}
        @endforeach
        {{ $comments->links() }}
        @if (Auth::user())
        <!-- This is to display the new comments form -->
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">New Comment</h3>
            </div>
            <div class="panel-body">
                
                <!-- This form contains the original post that is being viewed. Below follows the comments attached to this post. -->
                 {{ Form::open(array('method' => 'POST', 'url' => secure_url('comment'), $post->id)) }}
                 <input type="hidden" name="postid" value="{{{ $post->id }}}">
                <!--{{ Form::open(array('action' => 'CommentsController@store'))}}-->
                    <div class="form-group @if ($errors->has('message')) has-error @endif">
                        {{ Form::label('message', 'Comment:') }}
                        {{ Form::textarea('message', null, array('class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Insert your comment here.')) }}
                    </div>
                    {{ Form::submit('Post', array('class'=>'btn btn-default')) }}
                {{ Form::close() }}
            </div>
        </div>
        @endif
    </div>
    
    <div class="col-sm-1" id="right">
    </div>
</div>
@stop