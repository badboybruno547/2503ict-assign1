<!-- This document contains all the additional code to add to the master blade file to create the view page. -->

<!-- imports the master blade file -->
@extends('layout.master')

<!-- imbeds the title of the individial page into the master. -->
@section('title')
Mysface Friends List
@stop

<!-- Inserts the rest of the page into the body of the master page. -->
@section('content')

<!-- Creates the quick links table down the side. -->
<br/>
<div class="row">
    <div class="col-sm-3" id="left">
        <div class="panel panel-primary">
        <div class="panel-heading">Quick Links</div>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{{ URL::secure('/')}}}">Home</a></li>
                <li class="list-group-item"><a href="{{{ URL::secure('friend/people') }}}">People</a></li>
            </ul>
        </div>
    </div>
    
    <!-- creates the friends down the page -->
    <div class="col-sm-8" id="center">
        @if(count($friends) == 0)
        You have no friends. Click here to get some.
        {{ link_to(secure_url('friend/people'), 'Get Friends', array('class' => 'btn btn-default')) }}
        @endif
        @foreach ($friends as $friend)
        <!-- creates a friend for each friend in the users friendslist. -->
         <div class="panel panel-primary">
            <div class="panel-body">
                @if($friend->image_file_name != null)
                        <a href="{{{ URL::secure('user', $friend->id) }}}"><img src="{{{ secure_asset($friend->image->url('thumb'))}}}"></a>
                    @else
                        <a href="{{{ URL::secure('user', $friend->id) }}}"><img src="{{{ secure_asset('images/blankprofilepic.png') }}}" width="100" height ="100"></a>
                    @endif
                    <p></p>
                <div class="form-group">
                    
                    <label>Name:</label>
                    {{{ $friend->firstName }}} {{{ $friend->lastName }}}
                </div>
                <div class="form-group">
                    <label>Age:</label>
                    {{{ $friend->age }}}
                </div>
                {{ Form::open(array('method' => 'DELETE', 'url' => secure_url('friend/'.$friend->id))) }}
                    {{ Form::submit('Remove Friend', array('class'=>'btn btn-default')) }}
                {{ Form::close() }}
            </div>
        </div>
        @endforeach
        {{ $friends->links() }}
    </div>
    
    <div class="col-sm-1" id="right">
    </div>
</div>
@stop