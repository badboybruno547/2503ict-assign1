<!-- This document is the master blade file in which all other documents base their navbar and user photo from. Other forms add to this template file. -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@if(Auth::user())
{{{ Auth::user()->firstName }}}'s Profile
@endif</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    {{ HTML::style(secure_url('css/style.css')) }}

    <!-- Bootstrap core JavaScript ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  </head>
  
<body>
    <div class="container">

      <!-- Mysface navbar -->
      <div class="row">
          <div class="col-sm-12">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{{ URL::secure('/')}}}">MysFace</a>
              </div>
              <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="{{{ URL::secure('user/documentation')}}}">Documentation</a></li>
                  @if (Auth::check())
                  <li><a href="{{{ URL::secure('user/logout')}}}">Logout</a></li>
                  @else
                  <li><a href="{{{ URL::secure('user/loginform')}}}">Login</a></li>
                  <li><a href="{{{ URL::secure('user/create')}}}">Register</a></li>
                  @endif
                </ul>
              </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
          </nav>
          </div>
      </div>
      <!-- Adds the user's photo to the page. -->
    <div class="row">
      <div class="col-sm-3" id="left">
        @if($user->image_file_name != null)
          <img src="{{{ secure_asset($user->image->url('medium'))}}}">
          @else
          <img src="{{{ secure_asset('images/blankprofilepic.png') }}}" width="300" height ="300">
      @endif
      </div>
      <!-- adds the users name and other information to the page. -->
      <div class="col-sm-8" id="center">
          <div class="userName">
              <h1><a href="{{{ URL::secure('user', $user->id) }}}">{{{$user->firstName}}} {{{$user->lastName}}}</a></h1>
              <h3>Aged: {{{ $user->age }}}</h3>
          </div>
      </div>
    
      <div class="col-sm-1" id="right">
        
      </div>
    </div>
<br/>
<div class="row">
    <div class="col-sm-3" id="left">
        <div class="panel panel-primary">
        <div class="panel-heading">Quick Links</div>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{{ URL::secure('friend/people') }}}">People</a></li>
                @if(Auth::user())
                <li class="list-group-item"><a href="{{{ URL::secure('user/'.Auth::user()->id.'/edit') }}}">Edit My Profile</a></li>
                <li class="list-group-item"><a href="{{{ URL::secure('friend/') }}}">Friends</a></li>
                @endif
            </ul>
        </div>
    </div>
    
    <div class="col-sm-8" id="center">
        @if(Auth::check() && Auth::user()->id == $user->id)
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">New Post</h3>
            </div>
            <div class="panel-body">
                <!-- This form is the add new post form. it allows the user to create a new post from the home screen. -->
                {{ Form::open(array('method' => 'POST', 'url' => secure_url('post'))) }}
                    <div class="form-group @if ($errors->has('title')) has-error @endif">
                        {{ Form::label('title', 'Title:') }}
                        {{ Form::text('title', null, array('class'=>'form-control', 'placeholder'=>'Title of the post.')) }}
                        {{ $errors->first('title') }}
                    </div>
                    <div class="form-group @if ($errors->has('message')) has-error @endif">
                        {{ Form::label('message', 'Message:') }}
                        {{ Form::textarea('message', null, array('class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Insert your message here.')) }}
                        {{ $errors->first('message') }}
                    </div>
                    {{ Form::submit('Post', array('class'=>'btn btn-default')) }}
                {{ Form::close() }}
            </div>
        </div>
        @endif
        <!-- This code prints all the posts onto the page. Its data is based on an array provided by an sql query. It puts all posts in the database up in date order (newest first) -->
        
        @if ($posts)
            @for ($i = 0; $i < count($posts); $i++)
            <div class="panel panel-primary">
                <div class="panel-heading">
                    @if($posts[$i]->user->image_file_name != null)
                        <h3 class="panel-title"><img src="{{{ secure_asset($posts[$i]->user->image->url('thumb'))}}}">  {{{ $posts[$i]->user->firstName }}} {{{ $posts[$i]->user->lastName }}} posted at: {{{ $posts[$i]->created_at }}}</h3>
                    @else
                        <h3 class="panel-title"><img src="{{{ secure_asset('images/blankprofilepic.png') }}}" width="100" height ="100">  {{{ $posts[$i]->user->firstName }}} {{{ $posts[$i]->user->lastName }}} posted at: {{{ $posts[$i]->created_at }}}</h3>
                    @endif
                </div>
                <div class="panel-body">
                    {{ Form::model($posts[$i], array('method' => 'GET', 'route' => array('post.show', $posts[$i]->id))) }}
                        <div class="form-group">
                            {{ Form::label('title', 'Title: ', array('class'=>'messageTitle')) }}
                            {{ Form::label('title', $posts[$i]->title) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('textarea', 'Message: ', array('class'=>'messageTitle')) }}
                            {{ Form::label('textarea', $posts[$i]->message) }}
                        </div>
                        {{ Form::close() }}
                        <div class="form-group">
                            {{ link_to(secure_url('post/'.$posts[$i]->id), 'View '.$count[$i].' Comments', array('class' => 'btn btn-default')) }}
                            @if(Auth::check() && Auth::user()->id == $user->id)
                                {{ link_to(secure_url('post/'.$posts[$i]->id.'/edit'), 'Edit Post', array('class' => 'btn btn-default')) }}
                                {{ Form::open(array('method' => 'DELETE', 'url' => secure_url('post/'.$posts[$i]->id))) }}
                                    {{ Form::submit('Delete Post', array('class'=>'btn btn-default')) }}
                                {{ Form::close() }}
                            @endif
                        </div>
                </div>
            </div>
            @endfor
            {{ $posts->links() }}
        @endif
       
    </div>
    
    <div class="col-sm-1" id="right">
    </div>
  </div>
</body>

</html>