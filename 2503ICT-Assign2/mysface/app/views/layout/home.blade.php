<!-- This document contains all the additional code to add to the master blade file to create the home page. -->

<!-- imports the master blade file -->
@extends('layout.master')

<!-- imbeds the title of the individial page into the master. -->
@section('title')
Mysface Home
@stop

<!-- Inserts the rest of the page into the body of the master page. -->
@section('content')

<!-- Creates the quick links table down the side. -->
<br/>
<div class="row">
    <div class="col-sm-3" id="left">
        <div class="panel panel-primary">
        <div class="panel-heading">Quick Links</div>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{{ URL::secure('friend/people') }}}">People</a></li>
                @if(Auth::user())
                <li class="list-group-item"><a href="{{{ URL::secure('friend/') }}}">Friends</a></li>
                @endif
            </ul>
        </div>
    </div>
    
    <div class="col-sm-8" id="center">
        @if(Auth::user())
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">New Post</h3>
            </div>
            <div class="panel-body">
                <!-- This form is the add new post form. it allows the user to create a new post from the home screen. -->
                {{ Form::open(array('method' => 'POST', 'url' => secure_url('post'))) }}
                    <div class="form-group @if ($errors->has('title')) has-error @endif">
                        {{ Form::label('title', 'Title:') }}
                        {{ Form::text('title', null, array('class'=>'form-control', 'placeholder'=>'Title of the post.')) }}
                        {{ $errors->first('title') }}
                    </div>
                    <div class="form-group @if ($errors->has('message')) has-error @endif">
                        {{ Form::label('message', 'Message:') }}
                        {{ Form::textarea('message', null, array('class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Insert your message here.')) }}
                        {{ $errors->first('message') }}
                    </div>
                    {{ Form::submit('Post', array('class'=>'btn btn-default')) }}
                {{ Form::close() }}
            </div>
        </div>
        @endif
        <!-- This code prints all the posts onto the page. Its data is based on an array provided by an sql query. It puts all posts in the database up in date order (newest first) -->
        
        @if ($posts)
            @for ($i = 0; $i < count($posts); $i++)
            <div class="panel panel-primary">
                <div class="panel-heading">
                    @if($posts[$i]->user->image_file_name != null)
                        <h3 class="panel-title"><a href="{{{ URL::secure('user', $posts[$i]->user->id) }}}"><img src="{{{ secure_asset($posts[$i]->user->image->url('thumb'))}}}"></a>  {{{ $posts[$i]->user->firstName }}} {{{ $posts[$i]->user->lastName }}} posted at: {{{ $posts[$i]->created_at }}}</h3>
                    @else
                        <h3 class="panel-title"><a href="{{{ URL::secure('user', $posts[$i]->user->id) }}}"><img src="{{{ secure_asset('images/blankprofilepic.png') }}}" width="100" height ="100"></a>  {{{ $posts[$i]->user->firstName }}} {{{ $posts[$i]->user->lastName }}} posted at: {{{ $posts[$i]->created_at }}}</h3>
                    @endif
                </div>
                <div class="panel-body">
                        <div class="form-group">
                            <label>Title:</label>
                            {{{ $posts[$i]->title }}}
                        </div>
                        <div class="form-group">
                            <label>Message:</label>
                            {{{ $posts[$i]->message }}}
                        </div>
                        <div class="form-group">
                            {{ link_to(secure_url('post/'.$posts[$i]->id), 'View '.$count[$i].' Comments', array('class' => 'btn btn-default')) }}
                            @if(Auth::user())
                                {{ link_to(secure_url('post/'.$posts[$i]->id.'/edit'), 'Edit Post', array('class' => 'btn btn-default')) }}
                                {{ Form::open(array('method' => 'DELETE', 'url' => secure_url('post/'.$posts[$i]->id))) }}
                                    {{ Form::submit('Delete Post', array('class'=>'btn btn-default')) }}
                                {{ Form::close() }}
                            @endif
                        </div>
                </div>
            </div>
            @endfor
            {{ $posts->links() }}
        @endif
       
    </div>
    
    <div class="col-sm-1" id="right">
    </div>
@stop