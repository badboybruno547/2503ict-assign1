<!-- This document contains all the additional code to add to the master blade file to create the edit page. -->

<!-- imports the master blade file -->
@extends('layout.master')

<!-- imbeds the title of the individial page into the master. -->
@section('title')
Mysface Edit Post
@stop

<!-- Inserts the rest of the page into the body of the master page. -->
@section('content')

<!-- Creates the quick links table down the side. -->
<br/>
<div class="row">
    <div class="col-sm-3" id="left">
        <div class="panel panel-primary">
        <div class="panel-heading">Quick Links</div>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{{ URL::secure('/')}}}">Home</a></li>
                <li class="list-group-item"><a href="{{{ URL::secure('friend/people') }}}">People</a></li>
                @if(Auth::user())
                <li class="list-group-item"><a href="{{{ URL::secure('friend/') }}}">Friends</a></li>
                @endif
            </ul>
        </div>
    </div>
    
    <div class="col-sm-8" id="center">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Edit Post</h3>
            </div>
            <div class="panel-body">
                <!-- This form contains editable fields allowing the user to modify the title and content of the message but not the user that posted. -->
                {{ Form::model($post, array('method' => 'PUT', 'url' => secure_url('post/'.$post->id))) }}
                    <div class="form-group @if ($errors->has('title')) has-error @endif">
                        {{ Form::label('title', 'Title:') }}
                        {{ Form::text('title', null, array('class'=>'form-control', 'placeholder'=>'Title of the post.')) }}
                    </div>
                    <div class="form-group @if ($errors->has('message')) has-error @endif">
                        {{ Form::label('message', 'Message:') }}
                        {{ Form::textarea('message', null, array('class'=>'form-control', 'rows'=>'3', 'placeholder'=>'Insert your message here.')) }}
                    </div>
                    {{ Form::submit('Save Changes', array('class'=>'btn btn-default')) }}
                {{ Form::close() }}
                <div class="form-group submit_button">
                    {{ Form::open(array('method' => 'GET', 'url' => secure_url('/'))) }}
                        {{ Form::submit('Cancel Changes', array('class'=>'btn btn-default')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-sm-1" id="right">
    </div>
</div>
@stop