<!-- This document contains the html code to create the documents page. -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mysface Documentation</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript ===== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  </head>
  
<body>
    <div class="container">

      <!-- Navbar for Mysface -->
      <div class="row">
          <div class="col-sm-12">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{{ URL::secure('/')}}}">MysFace</a>
              </div>
              <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="{{{ URL::secure('user/documentation')}}}">Documentation</a></li>
                  @if (Auth::check())
                  <li><a href="{{{ URL::secure('user/logout')}}}">Logout</a></li>
                  @else
                  <li><a href="{{{ URL::secure('user/loginform')}}}">Login</a></li>
                  <li><a href="{{{ URL::secure('user/create')}}}">Register</a></li>
                  @endif
                </ul>
              </div>
            </div>
          </nav>
          </div>
      </div>
      
    <div class="row">
      <div class="col-sm-3" id="left">
      </div>
      <!-- content within the middle columnn of the website.-->
      <div class="col-sm-6" id="center">
        <h1 class="text-center">ER Diagram</h1>
            <img src="{{{secure_asset('images/er.png')}}}"></img>
        <h1 class="text-center">Site Diagram</h1>
            <img src="{{{secure_asset('images/site.png')}}}" class="text-center"></img>
        <h1 class="text-center">Documentation</h1>
        <br/>
            <p>I was unable to implement privacy due to lack of time to finish the assingment. Other than that everything else is working. An interesting approach
            to this is to actually use the eloquent framework. If setup properly it makes development of this very nice and easy. When it come to the friends, I actually
            implemented it correctly by only storing the data once in the friends table.</p>
            <br/>
            <p>PLEASE NOTE: under the app/database/seeders folder is where the seeder files are. These are the files to create the SQL database.</p>
            <br/><br/><br/><br/><br/><br/><br/>
      </div>
    
      <div class="col-sm-3" id="right">
      </div>
    </div>
  </div>
</body>

</html>