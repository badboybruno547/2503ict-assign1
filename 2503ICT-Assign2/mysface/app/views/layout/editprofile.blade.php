<!-- This document contains all the additional code to add to the master blade file to create the home page. -->

<!-- imports the master blade file -->
@extends('layout.master')

<!-- imbeds the title of the individial page into the master. -->
@section('title')
@if(Auth::user())
Edit {{{ Auth::user()->firstName }}}'s Profile
@endif
@stop

<!-- Inserts the rest of the page into the body of the master page. -->
@section('content')

<!-- Creates the quick links table down the side. -->
<br/>
<div class="row">
    <div class="col-sm-3" id="left">
        <div class="panel panel-primary">
        <div class="panel-heading">Quick Links</div>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{{ URL::secure('friend/people') }}}">People</a></li>
                @if(Auth::user())
                <li class="list-group-item"><a href="{{{ URL::secure('user/'.Auth::user()->id.'/edit') }}}">Edit My Profile</a></li>
                <li class="list-group-item"><a href="{{{ URL::secure('friend/') }}}">Friends</a></li>
                @endif
            </ul>
        </div>
    </div>
    
    <div class="col-sm-8" id="center">
        @if(Auth::user())
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Edit Account Information</h3>
            </div>
            <div class="panel-body">
                <!-- This form contains editable fields allowing the user to modify the users information. -->
                {{ Form::model(Auth::user(), array('method' => 'PUT', 'files' => true, 'url' => secure_url('user', Auth::user()->id))) }}
                    <div class="form-group @if ($errors->has('title')) has-error @endif">
                        {{ Form::label('firstName', 'First Name:') }}
                        {{ Form::text('firstName', null, array('class'=>'form-control', 'placeholder'=>'First Name')) }}
                        {{ $errors->first('firstName') }}
                    </div>
                    <div class="form-group @if ($errors->has('message')) has-error @endif">
                        {{ Form::label('lastName', 'Last Name') }}
                        {{ Form::text('lastName', null, array('class'=>'form-control', 'placeholder'=>'Last Name')) }}
                        {{ $errors->first('lastName') }}
                    </div>
                    <div class="form-group @if ($errors->has('message')) has-error @endif">
                        {{ Form::label('date', 'Date of Birth:') }}
                        {{ Form::text('dateOfBirth', null, array('class' => 'form-control','placeholder' => 'YYYY-MM-DD','data-datepicker' => 'datepicker')) }}
                        {{ $errors->first('dateOfBirth') }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('image', 'Profile Image:') }}
                        {{ Form::file('image') }}
                    </div>
                    {{ Form::submit('Update Details', array('class'=>'btn btn-default')) }}
                {{ Form::close() }}
                <div class="form-group submit_button">
                    {{ Form::open(array('method' => 'GET', 'url' => URL::secure('user',Auth::user()->id))) }}
                        {{ Form::submit('Cancel Changes', array('class'=>'btn btn-default')) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
        @endif
    </div>
    
    <div class="col-sm-1" id="right">
    </div>
@stop