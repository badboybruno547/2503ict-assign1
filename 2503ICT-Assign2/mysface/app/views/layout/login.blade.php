<!-- This document is the master blade file in which all other documents base their navbar and user photo from. Other forms add to this template file. -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    {{ HTML::style(secure_url('css/style.css')) }}

    <!-- Bootstrap core JavaScript ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  </head>
  
<body>
    <div class="container">

      <!-- Mysface navbar -->
      <div class="row">
          <div class="col-sm-12">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{{ URL::secure('/')}}}">MysFace</a>
              </div>
              <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="{{{ URL::secure('user/documentation')}}}">Documentation</a></li>
                  @if (Auth::check())
                  <li><a href="{{{ URL::secure('user/logout')}}}">Logout</a></li>
                  @else
                  <li><a href="{{{ URL::secure('user/loginform')}}}">Login</a></li>
                  <li><a href="{{{ URL::secure('user/create')}}}">Register</a></li>
                  @endif
                </ul>
              </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
          </nav>
          </div>
      </div>
      
      <div class="row">
      <div class="col-sm-4" id="left">
        
      </div>
      <!-- adds the users name and other information to the page. -->
      <div class="col-sm-4" id="center">
          <div class="userName">
              <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Login</h3>
            </div>
            <div class="panel-body">
                <!-- This form is the add new post form. it allows the user to create a new post from the home screen. -->
                {{ Form::open(array('url' => secure_url('user/login')))}}
                    <div class="form-group @if ($errors->has('title')) has-error @endif">
                        {{ Form::label('username', 'Email:') }}
                        {{ Form::text('username', null, array('class'=>'form-control', 'placeholder'=>'Email Address')) }}
                        {{ $errors->first('username') }}
                    </div>
                    <div class="form-group @if ($errors->has('message')) has-error @endif">
                        {{ Form::label('password', 'Password:') }}
                        {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
                        {{ $errors->first('password') }}
                    </div>
                    {{ Form::submit('Login', array('class'=>'btn btn-default')) }}
                    {{ link_to(secure_url('user/create'), 'Register', array('class' => 'btn btn-default')) }}
                {{ Form::close() }}
            </div>
        </div>
          </div>
      </div>
    
      <div class="col-sm-4" id="right">
        
      </div>
    </div>
  @section('content')
  @show
  </div>
</body>

</html>