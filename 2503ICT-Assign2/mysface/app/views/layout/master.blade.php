<!-- This document is the master blade file in which all other documents base their navbar and user photo from. Other forms add to this template file. -->

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    {{ HTML::style(secure_url('css/style.css')) }}

    <!-- Bootstrap core JavaScript ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  </head>
  
<body>
    <div class="container">

      <!-- Mysface navbar -->
      <div class="row">
          <div class="col-sm-12">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{{ URL::secure('/')}}}">MysFace</a>
              </div>
              <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="{{{ URL::secure('user/documentation')}}}">Documentation</a></li>
                  @if (Auth::check())
                  <li><a href="{{{ URL::secure('user/logout')}}}">Logout</a></li>
                  @else
                  <li><a href="{{{ URL::secure('user/loginform')}}}">Login</a></li>
                  <li><a href="{{{ URL::secure('user/create')}}}">Register</a></li>
                  @endif
                </ul>
              </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
          </nav>
          </div>
      </div>
      <!-- Adds the user's photo to the page. -->
      @if(Auth::check())
    <div class="row">
      <div class="col-sm-3" id="left">
        @if(Auth::User()->image_file_name != null)
          <img src="{{{ secure_asset(Auth::User()->image->url('medium'))}}}">
      @else
          <img src="{{{ secure_asset('images/blankprofilepic.png') }}}" width="300" height ="300">
      @endif
      </div>
      <!-- adds the users name and other information to the page. -->
      <div class="col-sm-8" id="center">
          <div class="userName">
              <h1><a href="{{{ URL::secure('user', Auth::user()->id) }}}">{{{Auth::user()->firstName}}} {{{Auth::user()->lastName}}}</a></h1>
              <h3>Aged: {{{ Auth::user()->age }}}</h3>
          </div>
      </div>
    
      <div class="col-sm-1" id="right">
        
      </div>
    </div>
    @else
    <div class="row">
      <div class="col-sm-3" id="left">
        <img src="{{{ secure_asset('images/blankprofilepic.png') }}}" width="300" height ="300">
      </div>
      <!-- adds the users name and other information to the page. -->
      <div class="col-sm-8" id="center">
          <div class="userName">
              <h1>Guest User</h1>
          </div>
      </div>
    
      <div class="col-sm-1" id="right">
        
      </div>
    </div>
    @endif
    <!-- marker that allows further content to be added to this master template file. -->
  @section('content')
  @show
  </div>
</body>

</html>