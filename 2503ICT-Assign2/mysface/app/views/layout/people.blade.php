<!-- This document contains all the additional code to add to the master blade file to create the view page. -->

<!-- imports the master blade file -->
@extends('layout.master')

<!-- imbeds the title of the individial page into the master. -->
@section('title')
Mysface Members List
@stop

<!-- Inserts the rest of the page into the body of the master page. -->
@section('content')

<!-- Creates the quick links table down the side. -->
<br/>
<div class="row">
    <div class="col-sm-3" id="left">
        <div class="panel panel-primary">
        <div class="panel-heading">Quick Links</div>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{{ URL::secure('/')}}}">Home</a></li>
                @if(Auth::user())
                <li class="list-group-item"><a href="{{{ URL::secure('friend') }}}">Friends</a></li>
                @endif
            </ul>
        </div>
    </div>
    
    <div class="col-sm-8" id="center">
        <!-- This for loop prints all of the comments liked to the above post. -->
        @foreach ($persons as $people)
        {{ Form::model($people, array('method' => 'POST', 'url' => secure_url('friend/create', $people->id))) }}
         <div class="panel panel-primary">
            <div class="panel-body">
                @if($people->image_file_name != null)
                        <a href="{{{ URL::secure('user', $people->id) }}}"><img src="{{{ secure_asset($people->image->url('thumb'))}}}"></a>
                    @else
                        <a href="{{{ URL::secure('user', $people->id) }}}"><img src="{{{ secure_asset('images/blankprofilepic.png') }}}" width="100" height ="100"></a>
                    @endif
                    <p></p>
                <!-- This form contains the original post that is being viewed. Below follows the comments attached to this post. -->
                <div class="form-group">
                    <label>Name:</label>
                    {{{ $people->firstName }}} {{{ $people->lastName }}}
                </div>
                <div class="form-group">
                    <label>Age:</label>
                    {{{ $people->age }}}
                </div>
                @if(Auth::user())
                {{ Form::submit('Add Friend', array('class'=>'btn btn-default')) }}
                @endif
            </div>
        </div>
        {{ Form::close() }}
        @endforeach
        {{ $persons->links() }}
    </div>
    
    <div class="col-sm-1" id="right">
    </div>
</div>
@stop