<?php

class Comment extends Eloquent 
{
    public static $rules = array(
            'message' => 'required'
    );
    function post()
    {
        return $this->belongsTo('Post');
    }
    function user()
    {
        return $this->belongsTo('User');
    }
}