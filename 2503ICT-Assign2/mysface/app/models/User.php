<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class User extends Eloquent implements UserInterface, RemindableInterface, StaplerableInterface
{
    use EloquentTrait;
	use UserTrait, RemindableTrait;
	public static $rules = array(
            'firstName' => 'required',
            'lastName' => 'required',
            'dateOfBirth' => 'required|date',
            'emailAddress' => 'required|unique:users|email',
            'password' => 'required|min:5'
        );
    public static $updaterules = array(
            'firstName' => 'required',
            'lastName' => 'required',
            'dateOfBirth' => 'required|date'
        );
        
    public function getAgeAttribute()
    {
        $birth_date = new DateTime($this->dateOfBirth);
        $todaysdate = new DateTime();
        $difference = $birth_date->diff($todaysdate);
        return $difference->y;
    }
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('image', ['styles' => ['medium' => '300x300', 'thumb' => '100x100']]);
        parent::__construct($attributes);
        
    }
	function posts()
    {
        return $this->hasMany('Post');
    }
    function comment()
    {
        return $this->hasMany('Comment');
    }
    function friendsAdded()
    {
    	return $this->belongsToMany('User', 'friends', 'user1_id', 'user2_id');
    }
    function addedFriends()
    {
    	return $this->belongsToMany('User', 'friends', 'user2_id', 'user1_id');
    }
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

}
