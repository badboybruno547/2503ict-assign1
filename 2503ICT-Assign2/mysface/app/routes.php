<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'UserController@index');

Route::post('friend/create/{id}', array('as' => 'friend.create', 'uses' => 'FriendsController@create'));
Route::get('friend/people', array('as' => 'friend.people', 'uses' => 'FriendsController@people'));
Route::get('user/documentation', array('as' => 'user.documentation', 'uses' => 'UserController@documentation'));
Route::get('user/loginform', array('as' => 'user.loginform', 'uses' => 'UserController@loginform'));
Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));

Route::resource('user', 'UserController');
Route::resource('post', 'PostController');
Route::resource('comment', 'CommentsController');
Route::resource('friend', 'FriendsController');