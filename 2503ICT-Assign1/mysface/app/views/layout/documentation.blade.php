<!-- This document contains the html code to create the documents page. -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mysface Documentation</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript ===== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  </head>
  
<body>
    <div class="container">

      <!-- Navbar for Mysface -->
      <div class="row">
          <div class="col-sm-12">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{{url("home")}}}">MysFace</a>
              </div>
              <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="documentation">Documentation</a></li>
                </ul>
              </div>
            </div>
          </nav>
          </div>
      </div>
      
    <div class="row">
      <div class="col-sm-3" id="left">
      </div>
      <!-- content within the middle columnn of the website.-->
      <div class="col-sm-6" id="center">
        <h1 class="text-center">ER Diagram</h1>
            <img src="images/er.png"></img>
        <h1 class="text-center">Site Diagram</h1>
            <img src="images/site.png" class="text-center"></img>
        <h1 class="text-center">Documentation</h1>
        <br/>
            <p>I was able to successfully implement all of the tasks in the project. 
            The form sanitisation was a bit difficult to understand and the links are very fiddley with the layers of webpages. 
            I was able to additionally implement a time stamp system, to store the time and date of when both posts and comments were created. 
            I was then able to sort posts and comments via the date/time instead of the ID number.</p>
            <br/>
            <p>PLEASE NOTE: under the app/database folder is a file called posts.sql. This is the file to create the SQL database.</p>
            <br/><br/><br/><br/><br/><br/><br/>
      </div>
    
      <div class="col-sm-3" id="right">
      </div>
    </div>
  </div>
</body>

</html>