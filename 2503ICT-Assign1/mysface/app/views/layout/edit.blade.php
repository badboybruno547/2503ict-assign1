<!-- This document contains all the additional code to add to the master blade file to create the edit page. -->

<!-- imports the master blade file -->
@extends('layout.master')

<!-- imbeds the title of the individial page into the master. -->
@section('title')
Mysface Friends
@stop

<!-- Inserts the rest of the page into the body of the master page. -->
@section('content')
<!-- Creates the quick links table down the side. -->
<br/>
<div class="row">
    <div class="col-sm-3" id="left">
        <div class="panel panel-primary">
        <div class="panel-heading">Quick Links</div>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{{url("home")}}}">Home</a></li>
                <li class="list-group-item"><a href="./">Friends</a></li>
                <li class="list-group-item"><a href="./">Groups</a></li>
            </ul>
        </div>
    </div>
    
    <div class="col-sm-8" id="center">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Edit Post</h3>
            </div>
            <div class="panel-body">
                <!-- This form contains editable fields allowing the user to modify the title and content of the message but not the user that posted. -->
            <form method="post" action="edit">
                <input type="hidden" name="postid" value="{{{ $post->id }}}">
                <div class="form-group">
                    <label for="exampleInputEmail1">User Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="{{{ $post->name }}}" readonly>
                </div>
                <div class="form-group @if ($errors->has('title')) has-error @endif">
                    <label for="exampleInputPassword1">Title:</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" name="title" value="{{{ $post->title }}}">
                    @if ($errors->has('title')) <p class="help-block">{{{ $errors->first('title') }}}</p> @endif
                </div>
                <div class="form-group @if ($errors->has('message')) has-error @endif">
                    <label for="messageTitle">Message:</label>
                    <textarea class="form-control" rows="3" name="message">{{{ $post->message }}}</textarea>
                    @if ($errors->has('message')) <p class="help-block">{{{ $errors->first('message') }}}</p> @endif
                </div>
                <button type="submit" class="btn btn-default" name="action" value="save">Save Post</button>
                <button type="submit" class="btn btn-default" name="action" value="cancel">Cancel Changes</button>
            </form>
            </div>
        </div>
    </div>
    
    <div class="col-sm-1" id="right">
    </div>
</div>
@stop