<!-- This document contains all the additional code to add to the master blade file to create the home page. -->

<!-- imports the master blade file -->
@extends('layout.master')

<!-- imbeds the title of the individial page into the master. -->
@section('title')
Mysface Home
@stop

<!-- Inserts the rest of the page into the body of the master page. -->
@section('content')

<!-- Creates the quick links table down the side. -->
<br/>
<div class="row">
    <div class="col-sm-3" id="left">
        <div class="panel panel-primary">
        <div class="panel-heading">Quick Links</div>
            <ul class="list-group">
                <li class="list-group-item"><a href="./">Friends</a></li>
                <li class="list-group-item"><a href="./">Groups</a></li>
            </ul>
        </div>
    </div>
    
    <div class="col-sm-8" id="center">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">New Post</h3>
            </div>
            <div class="panel-body">
                <!-- This form is the add new post form. it allows the user to create a new post from the home screen. -->
            <form action="add" method="post">
                <div class="form-group @if ($errors->has('name')) has-error @endif">
                    <label for="exampleInputEmail1">User Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="name" placeholder="Your Name." value="{{{ Input::old('name') }}}">
                    @if ($errors->has('name')) <p class="help-block">{{{ $errors->first('name') }}}</p> @endif
                </div>
                <div class="form-group @if ($errors->has('title')) has-error @endif">
                    <label for="exampleInputPassword1">Title:</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" name="title" placeholder="Title of the post." value="{{{ Input::old('title') }}}">
                    @if ($errors->has('title')) <p class="help-block">{{{ $errors->first('title') }}}</p> @endif
                </div>
                <div class="form-group @if ($errors->has('message')) has-error @endif">
                    <label for="messageTitle">Message:</label>
                    <textarea class="form-control" rows="3" name="message" placeholder="Insert your message here.">{{{ Input::old('message') }}}</textarea>
                    @if ($errors->has('message')) <p class="help-block">{{{ $errors->first('message') }}}</p> @endif
                </div>
                <button type="submit" class="btn btn-default" name="action" value="post">Post</button>
            </form>
            </div>
        </div>
        <!-- This code prints all the posts onto the page. Its data is based on an array provided by an sql query. It puts all posts in the database up in date order (newest first) -->
        @for ($i = 0; $i < count($post); $i++)
        <div class="panel panel-primary">
            
            <div class="panel-heading">
                
                <h3 class="panel-title"><img src="{{{url("images/icon.png")}}}" alt="Required Icon">  {{{ $post[$i]->name }}} posted on: {{{ $post[$i]->postTime }}}</h3>
            </div>
            <div class="panel-body">
                
            <form action="modify" method="get">
                <input type="hidden" name="id" value="{{{ $post[$i]->id }}}">
                <div class="form-group">
                    <label for="exampleInputPassword1">Title:</label>
                    {{{ $post[$i]->title }}}
                </div>
                <div class="form-group">
                    <label for="messageTitle">Message:</label>
                    {{{$post[$i]->message}}}
                </div>
                <button type="submit" class="btn btn-default" name="action" value="view">View {{{ $count[$i] }}} Comments</button>
                <button type="submit" class="btn btn-default" name="action" value="edit">Edit Post</button>
                <button type="submit" class="btn btn-default" name="action" value="delete">Delete</button>
            </form>
            </div>
        </div>
        @endfor
    </div>
    
    <div class="col-sm-1" id="right">
    </div>
@stop