<!-- This document contains all the additional code to add to the master blade file to create the view page. -->

<!-- imports the master blade file -->
@extends('layout.master')

<!-- imbeds the title of the individial page into the master. -->
@section('title')
Mysface Friends
@stop

<!-- Inserts the rest of the page into the body of the master page. -->
@section('content')

<!-- Creates the quick links table down the side. -->
<br/>
<div class="row">
    <div class="col-sm-3" id="left">
        <div class="panel panel-primary">
        <div class="panel-heading">Quick Links</div>
            <ul class="list-group">
                <li class="list-group-item"><a href="{{{url("home")}}}">Home</a></li>
                <li class="list-group-item"><a href="./">Friends</a></li>
                <li class="list-group-item"><a href="./">Groups</a></li>
            </ul>
        </div>
    </div>
    
    <div class="col-sm-8" id="center">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><img src="{{{url("images/icon.png")}}}" alt="Required Icon">   {{{ $user->firstName }}} {{{ $user->lastName }}} posted at: {{{ $post[0]->postTime }}}</h3>
            </div>
            <div class="panel-body">
                
                <!-- This form contains the original post that is being viewed. Below follows the comments attached to this post. -->
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">User Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" value="{{{ $post[0]->name }}}" readonly>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Title:</label>
                    <input type="text" class="form-control" id="exampleInputPassword1" value="{{{ $post[0]->title }}}" readonly>
                </div>
                <div class="form-group">
                    <label for="messageTitle">Message:</label>
                    <textarea class="form-control" rows="3" readonly>{{{ $post[0]->message }}}</textarea>
                </div>
            </form>
            </div>
        </div>
        
        <!-- This for loop prints all of the comments liked to the above post. -->
        @for ($i = 0; $i < count($comments); $i++)
        <form method="post" action="deleteComment">
            <div class="panel panel-primary">
                <input type="hidden" name="commentid" value="{{{ $comments[$i]->id }}}">
                <div class="panel-heading">
                    <button type="submit" class="close" aria-hidden="true" name="action" value="delete">&times;</button>
                    <h2 class="panel-title">{{{$comments[$i]->username}}}</h2><h6>commented on: {{{ $comments[$i]->commentTime }}}</h6>
                </div>
                <div class="panel-body">
                    {{{$comments[$i]->comment}}}
                </div>
            </div>
        </form>
        @endfor
        
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">New Comment</h3>
            </div>
            <div class="panel-body">
                <!-- This form is used to add a new comment to the post. -->
            <form method="post" action="addcomment">
                <input type="hidden" name="postid" value="{{{ $post[0]->id }}}">
                <div class="form-group @if ($errors->has('username')) has-error @endif">
                    <label for="exampleInputEmail1">User Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="username" placeholder="Your Name." value="{{{ Input::old('username') }}}">
                    @if ($errors->has('username')) <p class="help-block">{{{ $errors->first('username') }}}</p> @endif
                </div>
                <div class="form-group @if ($errors->has('message')) has-error @endif">
                    <label for="messageTitle">Message:</label>
                    <textarea class="form-control" rows="3" name="message" placeholder="Insert your comment here.">{{{ Input::old('message') }}}</textarea>
                    @if ($errors->has('message')) <p class="help-block">{{{ $errors->first('message') }}}</p> @endif
                </div>
                <button type="submit" class="btn btn-default" name="action" value="post">Post</button>
            </form>
            </div>
        </div>
    </div>
    
    <div class="col-sm-1" id="right">
    </div>
</div>
@stop