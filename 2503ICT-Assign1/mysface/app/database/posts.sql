/* Mysface database in SQLite. */
drop table if exists user;
drop table if exists posts;
drop table if exists comments;

create table user (
  id integer primary key autoincrement,
  firstName varchar(40) not null,
  lastName varchar(40) not null,
  icon varchar(40)
);

create table posts (
  id integer primary key autoincrement,
  name varchar(40),
  title varchar(40),
  message text,
  postTime varchar(40),
  userid int
);

create table comments (
  id integer primary key autoincrement,
  username varchar(40),
  comment text,
  commentTime varchar(40),
  postid int
);


/* Column names changed to avoid SQLite reserved words. */

/* Inserting sample data for users table */
insert into user(firstName, lastName, icon) values ('Brenton','Millers','Brenton.jpg');

/* Inserting sample data for posts table */
insert into posts(name, title, message, postTime, userid) values ('Brenton Millers','Hi Everybody','I would just like to welcome everybody to Mysface. I hope you like it.','2015-04-21 21:27:35','1');
insert into posts(name, title, message, postTime, userid) values ('Brenton Millers','WOOOO!!!','My amazing lecturer David Chen gave me 100% on my assignment xD!!','2015-04-22 15:17:37','1');

/* Inserting sample data for the comments table */

insert into comments(username, comment, commentTime, postid) values ('David Chen','Well done, it looks fantastic.','2015-04-21 21:30:35','1');
insert into comments(username, comment, commentTime, postid) values ('Bradley Gardam','This is as good as Myspace and Facebook Combined.','2015-04-21 22:32:50','1');
insert into comments(username, comment, commentTime, postid) values ('David Chen','It was a well deserved mark. Your website is great. Keep up the great work Brenton.','2015-04-22 15:20:29','2');
insert into comments(username, comment, commentTime, postid) values ('Bradley Gardam','Brenton, you are so much better at uni than I am.','2015-04-22 15:32:53','2');
insert into comments(username, comment, commentTime, postid) values ('Brentons Mummy','Im so proud of you son.','2015-04-22 15:55:03','2');

/* Sample query to test insertion worked with the user table */
select * from user
where firstName like "Brenton"
order by id;

/* Sample query to test insertion worked with the posts table. */
select * from posts
where id = 2;

/* Sample query to test insertion worked with the comments table. */
select * from comments
where username like "%David%"
order by id;
