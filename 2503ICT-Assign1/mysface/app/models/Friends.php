<?php

function getFriendsList()
{
    $friends = array();
    $friends[0] = array('name' => 'Brad', 'picLocation' => 'images/Brad.png', 'friendsSince' => '22/03/2013');
    $friends[1] = array('name' => 'Brenton', 'picLocation' => 'images/Brenton.jpg', 'friendsSince' => '18/09/2012');
    $friends[2] = array('name' => 'Sam', 'picLocation' => 'images/Sam.png', 'friendsSince' => '30/12/2009');
    $friends[3] = array('name' => 'Hatman', 'picLocation' => 'images/Hatman.png', 'friendsSince' => '07/06/2014');
    $friends[4] = array('name' => 'Tyson', 'picLocation' => 'images/Tyson.png', 'friendsSince' => '26/09/1994');
    return $friends;
}

?>