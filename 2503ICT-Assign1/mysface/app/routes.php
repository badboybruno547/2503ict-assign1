<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//The main entry point to the routes file. This grabs the user information, all posts for the user and the count of all
//of the comments and parses them through to the view to prepopulate the data in the forms/template.
Route::get('/', function()
{
	$user = get_User();
	$post = get_Posts($user[0]->id);
	$count = array();
	for($i = 0; $i < count($post); $i++)
	{
		$count[] = get_Number_Of_Comments_For_Post($post[$i]->id);
	}
	return View::make('layout.home')->withUser($user[0])->withPost($post)->withCount($count);
});

//This is the route to link to the documentation for the assignment.
Route::get('documentation', function()
{
	return View::make('layout.documentation');
});

//This is the route for the post method for the (add new post) form on the home page. It gets all the fields out, validates them
//and parses them through to the view via redirection to the get route. The data is also written to the database.
Route::post('/{post}', function($post)
{
	$name = Input::get('name');
	$title = Input::get('title');
	$message = Input::get('message');
	
	$validator = Validator::make(
    array(
		'name' => $name,
        'title' => $title,
        'message' => $message
    ),
    array(
        'name' => 'required',
        'title' => 'required',
        'message' => 'required'
    )
	);
	
	if ($validator->fails())
	{
		return Redirect::to(url("./"))->withErrors($validator)->withInput(Input::except(''));
	}
	else
	{
		$userid = get_User();
		$postTime = Carbon\Carbon::now();
		add_Post($name, $title, $message, $postTime, $userid[0]->id);
		return Redirect::to(url("./"));
	}
});

//this is the route for the edit page. This gets the users information, gets the post for the id that is provided
//in the url and parses this information through to the view.
Route::get('edit/{id}', function($id)
{
    $user = get_User();
    $post = get_Post($id);
	return View::make('layout.edit')->withUser($user[0])->withPost($post[0]);
});

//This is the route for the post method for the (edit post) form on the edit page. It gets all the fields out, validates them
//and parses them through to the view via redirection to the get route. The data is also written to the database.
Route::post('edit/{id}', function($id)
{
	$action = Input::get('action');
	if ($action == 'save')
	{
		$postid = Input::get('postid');
		$name = Input::get('name');
		$title = Input::get('title');
		$message = Input::get('message');
		
		$validator = Validator::make(
	    array(
    		'name' => $name,
	        'title' => $title,
	        'message' => $message
	    ),
	    array(
	        'name' => 'required',
	        'title' => 'required',
	        'message' => 'required'
	    )
		);
		
		if ($validator->fails())
		{
			return Redirect::to(url("edit/{$postid}"))->withErrors($validator)->withInput(Input::except(''));
		}
		else
		{
			$condition = update_Post($name, $title, $message, $postid);
			return Redirect::to(url("view/{$postid}"));
		}
	}
	else //cancel button was pressed
	{
		return Redirect::to(url("./"));
	}
});

//this route returns the caller to the home page. This helps get around "Secure Page" issue.
Route::get('home', function()
{
	return Redirect::to(url("./"));
});

//This route creates the view page based on a post id provided in with the url. It gets the users data, the post information and all 
//of the posts comments and displays them all on the view.
Route::get('view/{id}', function($id)
{
	$user = get_User();
	$post = get_Post($id);
	$comments = get_Comments_For_Post($post[0]->id);
	return View::make('layout.view')->withUser($user[0])->withPost($post)->withComments($comments);
});

//This is the route for the post method for the (view post) form on the view page. This route performs one of two things based on the action provided.
//It will either add or delete a comment. It gets the postid, username, message and comment time, validates it
//and writes the results to the data base. It then redirects to itself.
Route::post('view/{id}', function($id)
{
	if ($id == 'addcomment')
	{
		$postid = Input::get('postid');
		$username = Input::get('username');
		$message = Input::get('message');
		$commentTime = Carbon\Carbon::now();
		
		$validator = Validator::make(
	    array(
    		'username' => $username,
	        'message' => $message
	    ),
	    array(
	        'username' => 'required',
	        'message' => 'required'
	    )
		);
		
		if ($validator->fails())
		{
			return Redirect::to(url("view/{$postid}"))->withErrors($validator)->withInput(Input::except(''));
		}
		else
		{
			add_Comment($username, $message, $commentTime, $postid);
			return Redirect::to(url("view/{$postid}"));
		}
	}
	else if ($id == 'deleteComment')
	{
		$commentid = Input::get('commentid');
		$postids = get_Postid_From_Comment($commentid);
		$deleted = delete_Comment($commentid);
		if ($deleted)
		{
			return Redirect::to(url("view/{$postids[0]->postid}"));
		}
		else
		{
			die("Error deleting comment.");
		}
	}
});

//This route gets the modify command from a form on the home page. It bases its actions on what is parsed in by the form. if the button
//pressed is the delete button, it will delete both the post and the comments. if its view or edit, a redirection occurs to create these pages.
Route::get('modify', function()
{
  $btnpressed = Input::get('action');
  $id = Input::get('id');
  
	if ($btnpressed == 'delete')
	{
		$value = delete_Post($id);
		if ($value)
		{
			delete_Comments($id);
			return Redirect::to(url("./"));
		}
		else
		{
			die("Could not delete post and its comments.");
		}
	}
	else
	{
	    return Redirect::to(url("$btnpressed/$id"));    
	}
});

//This function deletes a post based on an id.
function delete_Post($id)
{
	$sql = "DELETE FROM posts where posts.id = ?";
	$item = DB::update($sql, array($id));
	return $item;
}

//This function deletes all the comments from a specific id. 
function delete_Comments($id)
{
	$sql = "DELETE FROM comments where comments.postid = ?";
	$item = DB::update($sql, array($id));
	return $item;
}

//This function updates the changes to a post.
function update_Post($name, $title, $message, $id)
{
	$sql = "UPDATE posts SET name=?, title=?, message=? WHERE id = ?";
	$item = DB::update($sql, array($name, $title, $message, $id));
	return $item;
}

//This function gets the postid based on a commentid.
function get_Postid_From_Comment($commentid)
{
	$sql = "SELECT postid FROM comments WHERE comments.id = ?";
	$id = DB::select($sql, array($commentid));
	return $id;
}

//This function deletes a single comment from a commentid.
function delete_Comment($commentid)
{
	$sql = "DELETE FROM comments WHERE id = ?";
	$id = DB::delete($sql, array($commentid));
	return $id;
}

//This function adds a new comment to the comments table.
function add_Comment($username, $message, $commentTime, $postid)
{
	$sql = "INSERT INTO comments (username, comment, commentTime, postid) values (?, ?, ?, ?)";
	DB::insert($sql, array($username, $message, $commentTime, $postid));
	$id = DB::getPdo()->lastInsertId();
	return $id;
}

//This function adds a new post to the posts table.
function add_Post($name, $title, $message, $postTime, $userid)
{
	$sql = "INSERT INTO posts (name, title, message, postTime, userid) values (?, ?, ?, ?, ?)";
	DB::insert($sql, array($name, $title, $message, $postTime, $userid));
	$id = DB::getPdo()->lastInsertId();
	return $id;
}

//This function counts the number of comments for a post based on the id of the post.
function get_Number_Of_Comments_For_Post($id)
{
	$sql = "SELECT comments.id FROM comments, posts WHERE comments.postid = posts.id AND posts.id = ?";
	$item = DB::select($sql, array($id));
	return count($item);
}

//This function gets all the data about the user.
function get_User()
{
	$sql = "SELECT * FROM user";
	$item = DB::select($sql);
	return $item;
}

//This function gets the post based on a provided postid.
function get_Post($id)
{
	$sql = "SELECT * FROM posts WHERE id = ?";
	$item = DB::select($sql, array("$id"));
	return $item;
}

//This function gets all posts from a specific user and returns them in decending order.
function get_Posts($id)
{
	$sql = "SELECT * FROM posts WHERE userid = ? ORDER BY posts.postTime DESC";
	$item = DB::select($sql, array("$id"));
	return $item;
}

//This function gets the comments for a post given a postid.
function get_Comments_For_Post($id)
{
	$sql = "SELECT comments.* FROM comments, posts WHERE posts.id = ? AND comments.postid = posts.id ORDER BY comments.commentTime";
	$item = DB::select($sql, array("$id"));
	return $item;
}
